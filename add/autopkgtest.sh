#!/bin/bash
set -e

pkg=epcr

if [ "${AUTOPKGTEST_TMP}" = "" ] ; then
  AUTOPKGTEST_TMP=$(mktemp -d /tmp/${pkg}-test.XXXXXX)
  # Double quote below to expand the temporary directory variable now versus
  # later is on purpose.
  # shellcheck disable=SC2064
  trap "rm -rf ${AUTOPKGTEST_TMP}" 0 INT QUIT ABRT PIPE TERM
fi

cp -a /usr/share/doc/${pkg}/examples/* "${AUTOPKGTEST_TMP}"

cd "${AUTOPKGTEST_TMP}"

#do_stuff_to_test_package#
export LANG=de_DE.UTF-8
export LC_CTYPE=de_DE.UTF-8
export LANGUAGE=de_DE.UTF-8


chsh -s /bin/dash
echo Success