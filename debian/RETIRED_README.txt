......................................................................
       README for ftp://ftp.ncbi.nlm.nih.gov/pub/schuler/e-PCR/
......................................................................

The e-PCR suite of tools has been retired and the content of this FTP 
direcory has been removed.

Please consider using PrimerBLAST as an alternative if you are 
interested in designing PCR primers or identifying target sequences 
for your primers. Primer-BLAST web page:
https://www.ncbi.nlm.nih.gov/tools/primer-blast/index.cgi

For more information, see "Electronic-PCR (e-PCR) is retiring, use 
Primer-BLAST instead" at the NCBI Insights blog:
https://ncbiinsights.ncbi.nlm.nih.gov/2017/06/28/e-pcr-is-retiring-use-primer-blast

______________________________________________________________________
National Center for Biotechnology Information (NCBI)
National Library of Medicine
National Institutes of Health
8600 Rockville Pike
Bethesda, MD 20894, USA
tel: (301) 496-2475
fax: (301) 480-9241
e-mail: info@ncbi.nlm.nih.gov
______________________________________________________________________
