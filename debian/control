Source: epcr
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Steffen Moeller <moeller@debian.org>,
           Andreas Tille <tille@debian.org>,
           Charles Plessy <plessy@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper (>= 12~)
Standards-Version: 4.3.0
Vcs-Browser: https://salsa.debian.org/med-team/epcr
Vcs-Git: https://salsa.debian.org/med-team/epcr.git
Homepage: https://www.ncbi.nlm.nih.gov/tools/epcr/

Package: ncbi-epcr
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Suggests: bioperl
Description: Tool to test a DNA sequence for the presence of sequence tagged sites
 Electronic PCR (e-PCR) is computational procedure that is used to identify
 sequence tagged sites(STSs), within DNA sequences. e-PCR looks for potential
 STSs in DNA sequences by searching for subsequences that closely match the
 PCR primers and have the correct order, orientation, and spacing that could
 represent the PCR primers used to generate known STSs.
 .
 The new version of e-PCR implements a fuzzy matching strategy. To reduce
 likelihood that a true STS will be missed due to mismatches, multiple
 discontiguous words may be used instead of a single exact word. Each of this
 word has groups of significant positions separated by 'wildcard' positions
 that are not required to match. In addition, it is also possible to allow
 gaps in the primer alignments.
 .
 The main motivation for implementing reverse searching (called Reverse e-PCR)
 was to make it feasible to search the human genome sequence and other large
 genomes. The new version of e-PCR provides a search mode using a query
 sequence against a sequence database.
 .
 This program is retired upstream and it is suggested to use Primer-Blast
  https://www.ncbi.nlm.nih.gov/tools/primer-blast/
 instead.
